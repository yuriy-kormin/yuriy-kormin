<!-- ![](https://visitor-badge.glitch.me/badge?page_id=yuriy-kormin.yuriy.kormin) -->
## VISITORS ![Visitor Count](https://profile-counter.glitch.me/yuriy-kormin/count.svg?label=visitors&style=plastic)

&#9996; Hello! My name is Yuriy. I'm python developer with IoT xp
<p>Preffer to live in Batumi, Georgia :georgia: seacost
<p>
Since <a href="https://ru.hexlet.io/u/tork">successfully completed</a> Python developer course at <a href = https://en.hexlet.io/u/tork>Hexlet</a> <p>

Tried myself in arduino / raspberry pi (IoT) for homemade.
  
Of the achievements - a home-made solid fuel boiler control system using several stepper motors and a relay to turn on / off ventilation system. 
This made in python with multithreading elements. The Zabbix monitoring system was connected to this whole thing.
  
More details can be found in my profile at <a href = "https://www.upwork.com/freelancers/~013e3d9819e16cfade?s=1110580753891577856">upwork</a>(need to register)<p>
<br>
  
🔭 Actively looking for a job (internship) in the field of backend in python (mostly remote plan)

  
## ʟᴀɴɢᴜᴀɢᴇꜱ ᴀɴᴅ ᴛᴏᴏʟꜱ

![Python](https://img.shields.io/badge/-Python-3776AB?style=plastic&logo=python&logoColor=white)
![Django](https://img.shields.io/badge/-Django-092E20?logo=django&?style=plastic&logoColor=white)
![Flask](https://img.shields.io/badge/flask-%23FFFFFF.svg?style=plastic&logo=flask&logoColor=black)
![Celery](https://img.shields.io/badge/-Celery-37814A?logo=celery&style=plastic&logoColor=white)
![GraphQL](https://img.shields.io/badge/-GraphQL-E10098?style=plastic&logo=graphql&logoColor=white)

![PyCharm](https://img.shields.io/badge/pycharm-%23000.svg?style=plastic&logo=pycharm&color=brightgreen)
![Poetry](https://img.shields.io/badge/poetry-%231A1A1A.svg?style=plastic&logo=python&logoColor=white)
![Ubuntu](https://img.shields.io/badge/-Ubuntu-E95420?logo=ubuntu&style=plastic&logoColor=white)
![Ansible](https://img.shields.io/badge/-Ansible-EE0000?logo=ansible&style=plastic&logoColor=white)
![Vagrant](https://img.shields.io/badge/vagrant-%231563FF.svg?style=plastic&logo=vagrant&logoColor=white)
 
![PostgreSQL](https://img.shields.io/badge/-PostgreSQL-4169E1?logo=postgresql&style=plastic&logoColor=white)
![PostGIS](https://img.shields.io/badge/-PostGIS-4169E1?logo=postgis&style=plastic&logoColor=white)
![Redis](https://img.shields.io/badge/-Redis-DC382D?style=plastic&logo=redis&logoColor=white)

![CI/CD](https://img.shields.io/badge/CI/CD-%23323330.svg?style=plastic&logo=dev.to&logoColor=white)
![pytest](https://img.shields.io/badge/pytest-%2300A3E0.svg?style=plastic&logo=pytest&logoColor=white)
![GitHub Actions](https://img.shields.io/badge/github_actions-%232088FF.svg?style=plastic&logo=github-actions&logoColor=white)
![Code Climate](https://img.shields.io/badge/code_climate-%233776AB.svg?style=plastic&logo=code-climate&logoColor=white)
  
![HTML](https://img.shields.io/badge/-HTML-E34F26?logo=html5&style=plastic&logoColor=white)
![CSS](https://img.shields.io/badge/-CSS-1572B6?logo=css3&style=plastic&logoColor=white)
![Requests](https://img.shields.io/badge/-Requests-0087EA?logo=requests&style=plastic&logoColor=white)
![BeautifulSoup](https://img.shields.io/badge/-BeautifulSoup-59666C?logo=beautifulsoup&style=plastic&logoColor=white)
![Scrapy](https://img.shields.io/badge/-Scrapy-000000?style=plastic&logo=scrapy&logoColor=white)
 
![Docker](https://img.shields.io/badge/-Docker-2496ED?logo=docker&style=plastic&logoColor=white)
![Docker Compose](https://img.shields.io/badge/docker_compose-%232496ED.svg?style=plastic&logo=docker&logoColor=white)
![Docker Hub](https://img.shields.io/badge/docker_hub-%230db7ed.svg?style=plastic&logo=docker&logoColor=white)

## STATS

![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=yuriy-kormin&theme=transparent&layout=compact&hide_border=true)
![My GitHub stats](https://github-readme-stats.vercel.app/api?username=yuriy-kormin&show_icons=true&hide_title=true&layout=compact&theme=transparent&hide_border=true&)

### Last week stat by [Wakatime](https://wakatime.com/)
<!--START_SECTION:waka-->

```text
Other                      18 hrs          ███████████████▒░░░░░░░░░   61.95 %
Python                     8 hrs 39 mins   ███████▒░░░░░░░░░░░░░░░░░   29.80 %
.env file                  42 mins         ▓░░░░░░░░░░░░░░░░░░░░░░░░   02.43 %
HTML                       30 mins         ▒░░░░░░░░░░░░░░░░░░░░░░░░   01.75 %
YAML                       28 mins         ▒░░░░░░░░░░░░░░░░░░░░░░░░   01.62 %
```

<!--END_SECTION:waka-->
